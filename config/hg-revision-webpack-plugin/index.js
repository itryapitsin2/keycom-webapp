var buildFile = require('./build-file');
var hgCommand = require('./helpers/run-hg-command');

var COMMIT_HASH_COMMAND = 'id -i';
var BRANCH_COMMAND = 'identify -b';

function HgRevisionPlugin (options) {
    options = options || {};

    this.commithashCommand = options.commithashCommand ||
        COMMIT_HASH_COMMAND;

    this.branchCommand = options.branchCommand ||
        BRANCH_COMMAND;
}

HgRevisionPlugin.prototype.apply = function (compiler) {
    debugger;

    buildFile({
        compiler: compiler,
        command: this.commithashCommand,
        asset: 'COMMITHASH',
        replacePattern: /\[hg-revision-hash\]/gi,
    });
    //
    // buildFile({
    //     compiler: compiler,
    //     hgWorkTree: this.hgWorkTree,
    //     command: this.versionCommand,
    //     replacePattern: /\[git-revision-version\]/gi,
    //     asset: 'VERSION'
    // });
    //
    // if (this.createBranchFile) {
    //     buildFile({
    //         compiler: compiler,
    //         hgWorkTree: this.hgWorkTree,
    //         command: this.branchCommand,
    //         replacePattern: /\[git-revision-branch\]/gi,
    //         asset: 'BRANCH'
    //     })
    // }
};

HgRevisionPlugin.prototype.commithash = function () {
    try {
        return hgCommand(
            this.commithashCommand
        )
    } catch (e) {
        console.log(e);
        return "";
    }
};

// HgRevisionPlugin.prototype.version = function () {
//     return hgCommand(
//         this.versionCo
//     )
// };

HgRevisionPlugin.prototype.branch = function () {
    try {
    return hgCommand(
        this.branchCommand
    )

    } catch (e) {
        console.log(e);
        return "";
    }
};

module.exports = HgRevisionPlugin;
