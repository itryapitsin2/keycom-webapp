const webpack = require('webpack');
const CleanWebPackPlugin = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const HgRevisionPlugin = require('./hg-revision-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const commonPaths = require('./common-paths');

const hgRevisionPlugin = new HgRevisionPlugin();

const config = {
    entry: {
        bundle: './src/index.tsx',
    },
    output: {
        filename: '[name].js',
        path: commonPaths.outputPath,
    },
    module: {
        rules: [{
            test: /\.(ts|tsx)$/,
            loader: 'awesome-typescript-loader'
        }, {
            test: /\.s?css$/,
            use: ExtractTextWebpackPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'sass-loader',
                    },
                ],
            }),
        }, {
            test: /\.svg|.png|.jpg$/,
            loader: 'url-loader',
            exclude: /node_modules/,
        }, {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: 'url-loader?limit=10000&mimetype=application/font-woff',
        }, {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: 'file-loader',
        }],
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new ExtractTextWebpackPlugin('styles.css'),
        new CleanWebPackPlugin(['public'], {root: commonPaths.root}),
        new HtmlWebPackPlugin({
            template: commonPaths.template,
            favicon: commonPaths.favicon,
            inject: true
        }),
    ],
};

module.exports = config;
