module.exports = {
    collectCoverageFrom: ["src/**/*.{js,jsx,ts,tsx}", "!src/**/*.d.ts"],
    roots: ["<rootDir>/src/"],
    testEnvironment: "jsdom",
    testPathIgnorePatterns: ["/dist/", "/node_modules/"],
    transform: {
        "^.+\\.tsx?$": "ts-jest",
    },
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    collectCoverage: true,
    setupFilesAfterEnv: ["<rootDir>src/setup-tests.ts"]
};
