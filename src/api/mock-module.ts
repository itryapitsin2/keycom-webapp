import { ContainerModule, interfaces } from 'inversify';

import { IQuickAddressSearchService } from './services/quick-address-search-service';
import { QuickAddressSearchServiceMock } from './services/mocks/quick-address-search-service';

export const MockApiModule = new ContainerModule((bind: interfaces.Bind): void => {
    bind<IQuickAddressSearchService>('QuickAddressSearchService').toConstantValue(
        QuickAddressSearchServiceMock(),
    );
});
