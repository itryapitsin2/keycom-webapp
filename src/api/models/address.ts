/**
 * REST API representation of Address
 * @interface Address
 */
import { serializable } from 'serializr';

export interface Address {
    /**
     * Record id
     * @type {string}
     * @memberof Address
     */
    id?: string;

    /**
     * City part of address
     * @type {string}
     * @memberof Address
     */
    city: string;

    lat: number;

    lon: number;

    name: string;

    sourceId?: string;

    streetName: string;

    streetNo: string;
}
//
// export class Address implements IAddress {
//     @serializable city: string;
//     @serializable id: string;
//     @serializable lat: number;
//     @serializable lon: number;
//     @serializable name: string;
//     @serializable sourceId: string;
//     @serializable streetName: string;
//     @serializable streetNo: string;
// }
