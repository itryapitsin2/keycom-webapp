import { ContainerModule, interfaces } from 'inversify';

import { IQuickAddressSearchService } from './services/quick-address-search-service';
import { QuickAddressSearchService } from './services/impl/quick-address-search-service';
import { IFavWindowService } from './services/fav-window-service';
import { FavWindowService } from './services/impl/fav-window-service';

export const ApiModule = new ContainerModule((bind: interfaces.Bind): void => {
    bind<IQuickAddressSearchService>('QuickAddressSearchService')
        .to(QuickAddressSearchService)
        .inSingletonScope();
    bind<IFavWindowService>('FavWindowService')
        .to(FavWindowService)
        .inSingletonScope();

    // bind<ThemeService>("ThemeService")
    //     .to(ThemeService)
    //     .inSingletonScope();
});
