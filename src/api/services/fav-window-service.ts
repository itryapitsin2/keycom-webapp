import { FavWindowElement } from '../../stores/footer/fav-window-element';

export interface IFavWindowService {
    getFavWindowElements(): FavWindowElement[];
}
