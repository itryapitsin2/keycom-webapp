import { IFavWindowService } from '../fav-window-service';
import { FavWindowElement } from '../../../stores/footer/fav-window-element';
import { Container, inject, injectable } from 'inversify';

@injectable()
export class FavWindowService implements IFavWindowService {
    private _container: Container;

    public constructor(@inject('Container') container: Container) {
        this._container = container;
    }

    public getFavWindowElements(): FavWindowElement[] {
        // this._container.get()
        return [];
    }
}
