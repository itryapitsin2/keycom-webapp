import 'reflect-metadata';
import { injectable } from 'inversify';
import { IQuickAddressSearchService } from '../quick-address-search-service';
import http from '../../../utils/http';
import { URL } from '../../../utils/settings';
import { ListResponse } from '../../models/list-response';
import { Address } from '../../models/address';

/**
 * Service API for `QuickAddressSearchController`
 */
@injectable()
export class QuickAddressSearchService implements IQuickAddressSearchService {
    public getAddressesByPattern = async (pattern: string): Promise<ListResponse<Address>> => {
        return await http(
            `${URL}/keygwt_address.quick_address_search/auto_search/?query=${pattern}`,
        )
            .catch((reason: any) => {
                console.log(reason);
                return {
                    items: [],
                    count: 0,
                };
            })
            .then((data: any) => {
                if (data.status !== 200) {
                    throw Error(`Request rejected with status ${data.status}`);
                }
                return {
                    items: data['addresses']
                        ? // Convert response to correct model structure
                          data['addresses'].map(
                              (address: Map<string, never>): Address => {
                                  return {
                                      id: address['id'],
                                      city: address['city'],
                                      lat: address['lat'],
                                      lon: address['lon'],
                                      name: address['name'],
                                      sourceId: address['source_id'],
                                      streetName: address['streetname'],
                                      streetNo: address['streetno'],
                                  };
                              },
                          )
                        : [],
                    count: data['totalCount'],
                };
            });
    };

    public locateToPoint = (lat: number, lon: number): void => {
        if (typeof keygwt !== 'undefined' && keygwt !== null) {
            const feature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(lon, lat));
            if (feature) {
                if (
                    keygwt.mapengine.zoomToGeometryWithScale(
                        keygwt.mapengine.map,
                        feature.geometry,
                        null,
                    )
                ) {
                    keygwt.mapengine.highlightFeature(feature);
                    keygwt.mapengine.systemMessage(gettext('Address located successfully.'));
                } else {
                    keygwt.mapengine.systemMessage(
                        gettext(
                            'Operation cannot be performed because object is outside of the map extent',
                        ),
                    );
                }
            }
            console.log(
                `Called QuickAddressSearchService#locateToPoint method with args: ${lat}, ${lon}`,
            );
        } else {
            console.log('Keygwt in not available');
        }
    };
}
