import 'reflect-metadata';

import { IQuickAddressSearchService } from '../quick-address-search-service';
import { ListResponse } from '../../models/list-response';
import { Address } from '../../models/address';

export const QuickAddressSearchServiceMock = jest.fn<IQuickAddressSearchService, []>(
    (): IQuickAddressSearchService => ({
        getAddressesByPattern: jest.fn(
            async (args: string): Promise<ListResponse<Address>> => {
                console.log(args);
                return {
                    items: [
                        {
                            id: '0',
                            city: 'Test city',
                            lat: 0,
                            lon: 0,
                            name: 'Test name',
                            streetName: 'test street name',
                            streetNo: '1',
                        },
                    ],
                    count: 1,
                };
            },
        ),
        locateToPoint: jest.fn(),
    }),
);
