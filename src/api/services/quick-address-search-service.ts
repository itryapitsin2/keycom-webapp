import { ListResponse } from '../models/list-response';
import { Address } from '../models/address';

/**
 * Quick Address Search service API specification
 */
export interface IQuickAddressSearchService {
    /**
     * Retrieve list of matched addresses by pattern
     * @param pattern {string} 'start with' pattern
     */
    getAddressesByPattern: (pattern: string) => Promise<ListResponse<Address>>;

    /**
     * Change position on map to latitude and longitude
     * @param lat {number} latitude
     * @param lon {number} longitude
     */
    locateToPoint: (lat: number, lon: number) => void;
}
