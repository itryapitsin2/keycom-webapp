// import {injectable} from "inversify";

// @injectable()
export class ThemeService {
    public static getDefaultTheme(): string {
        return 'default';
    }

    public static getCurrentTheme(): string {
        return ThemeService.getDefaultTheme();
    }
}
