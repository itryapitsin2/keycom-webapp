import { TopBarController } from '../../controllers/top-bar';
import { QuickAddressSearchWindowController } from '../../controllers/windows/quick-address-search-window';
import { FooterBarController } from '../../controllers/footer-bar';

export interface IApp {
    topBarController: TopBarController;

    quickAddressSearchWindowController: QuickAddressSearchWindowController;

    footerBarController: FooterBarController;
}
