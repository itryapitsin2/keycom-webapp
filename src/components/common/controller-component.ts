import { IBlockControllerProps } from '../props/controller-props';
import { ThemedBlock } from './themed-block';

/**
 * Base class for a block component
 *
 * @class ControllerComponent
 * @extends ThemedBlock<P>
 * @typeparam P extends `IControllerProps<C>`
 * @typeparam C is class of `Object`
 */
export class ControllerComponent<P extends IBlockControllerProps<C>, C> extends ThemedBlock<P> {
    /**
     * Component controller     *
     * @protected
     * @type {C}
     * @memberof ControllerComponent
     */
    protected controller: C;

    public constructor(props: P) {
        super(props);

        this.controller = props.controller;
    }
}
