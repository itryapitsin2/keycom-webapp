import React from 'react';
import { IDropdownProps } from './props';
import { ThemedBlock } from '../../themed-block';

export class Dropdown extends ThemedBlock<IDropdownProps> {
    public render(): React.ReactNode {
        const dropdown =
            this.props.items && this.props.items.length > 0 ? (
                <>
                    <button
                        type="button"
                        className="btn btn-secondary dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    ></button>
                    <div className="dropdown-menu dropdown-menu-right">
                        {this.props.items.map(item => (
                            <button
                                key={item}
                                className="dropdown-item"
                                type="button"
                                onClick={() => {
                                    this.props.onSelect(item);
                                }}
                            >
                                {item}
                            </button>
                        ))}
                    </div>
                </>
            ) : null;

        return (
            <div className="btn-group dropup">
                <button type="button" className="btn btn-secondary">
                    {this.props.selectedItem || 'Non'}
                </button>
                {dropdown}
            </div>
        );
    }
}
