import { IBlockProps } from '../../../props/block-props';

export interface IDropdownProps extends IBlockProps {
    items: any[];
    selectedItem: any;
    onSelect?: (any) => void;
}
