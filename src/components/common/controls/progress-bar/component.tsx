import React from 'react';
import { IProgressBar } from './props';
import { ThemedElement } from '../../themed-element';

export class ProgressBar extends ThemedElement<IProgressBar> {
    public render(): React.ReactNode {
        if (!this.props.show) {
            return null;
        }

        if (this.props.infinite) {
            return (
                <div className={this.elementClassName}>
                    <i className="fas fa-spinner fa-spin" />
                    <span>{this.props.text}</span>
                </div>
            );
        } else {
            return (
                <div className="progress">
                    <div
                        className="progress-bar"
                        role="progressbar"
                        style={{ width: this.props.value.toString() + '%' }}
                        aria-valuenow={25}
                        aria-valuemin={0}
                        aria-valuemax={100}
                    >
                        {this.props.value}% {this.props.text}
                    </div>
                </div>
            );
        }
    }
}
