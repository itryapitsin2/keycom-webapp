import { IElementProps } from '../../../props/element-props';

export interface IProgressBar extends IElementProps {
    text?: string;
    show?: boolean;
    infinite?: boolean;
    value?: number;
}
