import * as React from 'react';

export const FallbackLoader: React.FC<{}> = (): React.ReactElement => (
    <div className="app">
        <div>loading...</div>
    </div>
);
