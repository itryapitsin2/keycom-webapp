import L, { LeafletMouseEvent } from 'leaflet';
import { KeyEventHandler } from '../../../../utils/types';

type MapClickEventHandler = KeyEventHandler<LeafletMouseEvent>;
type MouseMoveOnMapHandler = KeyEventHandler<LeafletMouseEvent>;

class BaseDrawContainer extends L.Control {
    public map: L.Map;

    public layerGroup: L.LayerGroup;

    public onMapClick: MapClickEventHandler;

    public onMouseMoveOnMap: MouseMoveOnMapHandler;

    private readonly className: string;

    public constructor(className: string) {
        super();
        this.className = className;
    }

    public onAdd(map: L.Map): HTMLElement {
        const panelDiv = L.DomUtil.create('div', this.className);
        this.map = map;
        this.layerGroup = L.layerGroup([]).addTo(map);
        if (this.onMapClick) {
            this.map.on('click', this.onMapClick);
        }

        if (this.onMouseMoveOnMap) {
            this.map.on('mousemove', this.onMouseMoveOnMap);
        }
        return panelDiv;
    }

    public onRemove(map: L.Map): void {
        map.removeLayer(this.layerGroup);

        if (this.onMapClick) {
            this.map.off('click', this.onMapClick);
        }

        if (this.onMouseMoveOnMap) {
            this.map.off('mousemove', this.onMouseMoveOnMap);
        }
    }
}

export { BaseDrawContainer };
