import { MapControlProps } from 'react-leaflet';

export interface IBaseComponentProps {
    isDisplay: boolean;
}

export interface IBaseMapControlProps extends MapControlProps, IBaseComponentProps {}
