import L from 'leaflet';

type LMapControlCorner = {
    _controlCorners: object;
};

export class ControlContainer extends L.Control {
    public map: L.Map;

    private readonly className: string;

    public constructor(className: string) {
        super();
        this.className = className;
    }

    public onAdd(map: L.Map & LMapControlCorner): HTMLElement {
        const container = L.DomUtil.create(
            'div',
            `leaflet-bar easy-button-container leaflet-control ${this.className}`,
        );
        this.map = map;
        const pos = this.getPosition();
        const corner = map._controlCorners[pos];
        if (pos.indexOf('bottom') !== -1) {
            corner.insertBefore(container, corner.firstChild);
        } else {
            corner.appendChild(container);
        }
        return container;
    }
}
