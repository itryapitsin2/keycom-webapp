import * as Leaflet from 'leaflet';

import { SimpleMapControl } from './simple-map-control';
import { IBaseMapControlProps } from './base-map-control-props';

export class SimpleMapControlWithController<
    T extends Leaflet.Control,
    P extends IBaseMapControlProps & { controller: C },
    C = never
> extends SimpleMapControl<T, P> {
    protected controller: C;

    public constructor(props: P) {
        super(props);

        this.controller = props.controller;
    }
}
