import { MapControl } from 'react-leaflet';
import * as Leaflet from 'leaflet';
import { IBaseMapControlProps } from './base-map-control-props';
import { NotImplemented } from '../../../../utils/exceptions';

class SimpleMapControl<
    T extends Leaflet.Control,
    P extends IBaseMapControlProps = IBaseMapControlProps
> extends MapControl<P, T> {
    public createContainer(): T {
        throw new NotImplemented(`${this.constructor.name}.createContainer`);
    }

    public createLeafletElement(): T {
        this.leafletElement = this.createContainer();
        return this.leafletElement;
    }

    public componentDidMount(): void {
        const { map } = this.props.leaflet;
        this.leafletElement.addTo(map);
        this.forceUpdate();
    }

    public componentWillUnmount(): void {
        this.leafletElement.remove();
        this.forceUpdate();
    }

    // eslint-disable-next-line unicorn/prevent-abbreviations
    public componentWillReceiveProps(nextProperties): void {
        // Enable on map click handler, when component is activated
        if (nextProperties.isDisplay && !this.props.isDisplay) {
            this.leafletElement.addTo(this.props.leaflet.map);
        }

        // Disable on map click handler, when component is deactivated
        if (!nextProperties.isDisplay && this.props.isDisplay) {
            this.leafletElement.remove();
        }
    }
}

export { SimpleMapControl };
