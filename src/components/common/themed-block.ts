import React from 'react';
import { IThemedProps } from '../props/themed-props';
import { ThemeService } from '../../api/services/theme-service';
import { className } from '../../utils';
import { ClassNameFormatter } from '@bem-react/classname';
import { IBlockProps } from '../props/block-props';

export class ThemedBlock<T extends IBlockProps, S = {}> extends React.Component<T, S> {
    public static defaultProps: IThemedProps = {
        theme: ThemeService.getCurrentTheme(),
    };

    protected blockName: string;
    protected theme: string;
    protected blockNameFormatter: ClassNameFormatter;

    public constructor(props: T) {
        super(props);
        this.blockName = props.blockName;
        this.blockNameFormatter = className(this.blockName);
        this.theme = props.theme;
    }

    public get blockClassName(): string {
        return this.blockNameFormatter({ theme: this.theme }, [this.props.className]);
    }
}
