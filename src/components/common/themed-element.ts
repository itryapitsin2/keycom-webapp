import React from 'react';
import { IThemedProps } from '../props/themed-props';
import { ThemeService } from '../../api/services/theme-service';
import { className } from '../../utils';
import { ClassNameFormatter } from '@bem-react/classname';
import { IElementProps } from '../props/element-props';

export class ThemedElement<T extends IElementProps, S = {}> extends React.Component<T, S> {
    public static defaultProps: IThemedProps = {
        theme: ThemeService.getCurrentTheme(),
    };

    protected blockName: string;
    protected elementName: string;
    protected theme: string;
    protected elementFormatter: ClassNameFormatter;

    public constructor(props: T) {
        super(props);
        this.blockName = props.blockName;
        this.theme = props.theme;
        this.elementName = props.elementName;
        this.elementFormatter = className(this.blockName, this.elementName);
    }

    public get elementClassName(): string {
        const theme = this.props.theme;
        return this.elementFormatter({ theme }, [this.props.className]);
    }

    public getCustomElementClassName(element: string, theme?: string, mixes?: string[]): string {
        return className(this.blockName, element)({ theme }, mixes);
    }
}
