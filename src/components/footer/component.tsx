import React from 'react';

import { IFooterBarProps } from './props';
import { withTranslation } from 'react-i18next';
import { Dropdown } from '../common/controls/dropdown/component';
import { CoordinateReader } from './coordinate-reader';
import { FavWindowPanel } from './fav-window-panel';
import { ControllerComponent } from '../common/controller-component';
import { FooterBarController } from '../../controllers/footer-bar';
import { observer } from 'mobx-react';

@observer
class Component extends ControllerComponent<IFooterBarProps, FooterBarController> {
    public render(): React.ReactNode {
        return (
            <div className={`${this.blockClassName} fixed-bottom`}>
                <div className="navbar-nav mr-auto mt-2 mt-lg-0">
                    <FavWindowPanel
                        blockName="fav-window-panel"
                        elements={this.props.controller.favWindowElementStore.elements}
                        onDeleteElement={this.controller.onDeleteElement}
                    />
                </div>
                <form className="form-inline text-right">
                    <Dropdown items={[]} selectedItem={null} blockName={this.blockName} />
                    <input list="character" />
                    <datalist id="character">
                        <option value="Чебурашка"></option>
                        <option value="Крокодил Гена"></option>
                        <option value="Шапокляк"></option>
                    </datalist>
                    <CoordinateReader
                        blockName="coordinate-reader"
                        coordinateReaderLabel="XY:"
                        lat={0}
                        lon={0}
                    />
                </form>
            </div>
        );
    }
}

export const FooterBar = withTranslation()(Component);
