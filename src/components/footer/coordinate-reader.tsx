import React from 'react';
import { ICoordinateReaderProps } from './props';
import { ThemedBlock } from '../common/themed-block';
import { ClassNameFormatter } from '@bem-react/classname';
import { className } from '../../utils';

export class CoordinateReader extends ThemedBlock<ICoordinateReaderProps> {
    private readonly lableFormatter: ClassNameFormatter;
    private readonly inputFormatter: ClassNameFormatter;
    private readonly buttonFormatter: ClassNameFormatter;

    public constructor(props: ICoordinateReaderProps) {
        super(props);

        this.lableFormatter = className(this.blockName, 'label');
        this.inputFormatter = className(this.blockName, 'input');
        this.buttonFormatter = className(this.blockName, 'button');
    }

    public render(): React.ReactNode {
        return (
            <div className={this.blockClassName}>
                <span className={this.lableFormatter()}>
                    {this.props.coordinateReaderLabel}&nbsp;
                </span>
                <input
                    className={`${this.inputFormatter()} mr-sm-2`}
                    value={`${this.props.lat.toFixed(2)} ${this.props.lon.toFixed(2)} `}
                />
                <button className={this.buttonFormatter()}>
                    <i className="fas fa-globe-americas" />
                </button>
            </div>
        );
    }
}
