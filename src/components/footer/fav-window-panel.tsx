import * as React from 'react';
import { ThemedBlock } from '../common/themed-block';
import { IFavWindowPanelProps } from './props';
import { FavWindowElement } from '../../stores/footer/fav-window-element';
import { observer } from 'mobx-react';

const defaultIconClassName = 'fas fa-rocket';

@observer
export class FavWindowPanel extends ThemedBlock<IFavWindowPanelProps> {
    public render(): React.ReactNode {
        const elements = this.props.elements.map(
            (element: FavWindowElement, index: number): React.ReactNode => {
                const icon = element.iconClassName ? element.iconClassName : defaultIconClassName;

                return (
                    <>
                        <button
                            key={index}
                            className="btn btn-outline-success  dropdown-toggle"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            type="button"
                        >
                            <i className={icon} />
                        </button>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a
                                className="dropdown-item"
                                href="#"
                                onClick={(): void => {
                                    this.props.onDeleteElement(element);
                                }}
                            >
                                Delete all
                            </a>
                            {element.params.map(param => {
                                return (
                                    <a
                                        className="dropdown-item"
                                        href="#"
                                        onClick={(): void => {
                                            element.action(param.state);
                                        }}
                                    >
                                        {param.title}
                                    </a>
                                );
                            })}
                        </div>
                    </>
                );
            },
        );

        return (
            <div className={this.blockClassName}>
                <form className="form-inline">{elements}</form>
            </div>
        );
    }
}
