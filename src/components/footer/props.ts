import { WithTranslation } from 'react-i18next';
import { IBlockProps } from '../props/block-props';
import { FavWindowElement } from '../../stores/footer/fav-window-element';
import { IBlockControllerProps } from '../props/controller-props';
import { FooterBarController } from '../../controllers/footer-bar';

export interface ICoordinateReaderProps extends IBlockProps {
    coordinateReaderLabel?: string;
    lat?: number;
    lon?: number;
}

export interface IFooterBarProps
    extends IBlockControllerProps<FooterBarController>,
        WithTranslation {}

export interface IFavWindowPanelProps extends IBlockProps {
    elements: FavWindowElement[];

    onDeleteElement: (element: FavWindowElement) => void;
}
