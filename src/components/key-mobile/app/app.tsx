import * as React from 'react';
import {ClassNameFormatter} from '@bem-react/classname';
import {Map as LeafletMap, TileLayer, Circle} from 'react-leaflet';
import L from 'leaflet';

import {IKeyMobileAppProps} from './props';
import {className} from '../../../utils';
import {ThemedBlock} from '../../common/themed-block';
import MenuMapControl from '../map/menu-mapcontrol';
import {MenuItem} from '../map/menu-mapcontrol/menu-item';
import DistanceForm from '../forms/distance';
import {GeoCoordinates} from '../forms/geo-coordinates/geo-coordinates';
import {AddressSearchForm} from "../forms/address-search/component";

export class KeyMobileApp extends ThemedBlock<IKeyMobileAppProps> {
    private readonly keyMobileAppFormatter: ClassNameFormatter;

    public map: LeafletMap;

    public constructor(props: IKeyMobileAppProps) {
        super(props);
        this.keyMobileAppFormatter = className(this.blockName);
    }

    public render(): React.ReactNode {
        const position: L.LatLngExpression = [51.505, -0.09];
        return (
            <div className={this.keyMobileAppFormatter()}>
                <LeafletMap
                    center={position}
                    zoom={13}
                    ref={(map: LeafletMap): void => {
                        this.map = map;
                    }}
                >
                    <MenuMapControl isDisplay={true}>
                        <MenuItem
                            elementName="distance-calculator-item"
                            blockName="menu-map-control"
                            title="Distance calculator"
                        >
                            <div className="row">
                                <DistanceForm
                                    blockName="menu-map-control"
                                    distance={0}
                                    isPaused={false}
                                />
                            </div>
                        </MenuItem>
                        <MenuItem
                            title="Geo coordinates"
                            elementName="geo-coordinate-item"
                            blockName="menu-map-control"
                        >
                            <div className="row">
                                <GeoCoordinates
                                    blockName="menu-map-control"
                                    latitude={0}
                                    longitude={0}
                                    altitude={0}
                                    accuracy={0}
                                    isGeolocationAvailable={true}
                                    isGeolocationEnabled={true}
                                    hasGeolocationError={false}
                                />
                            </div>
                        </MenuItem>
                        <MenuItem
                            title="Tracker"
                            elementName="tracker-coordinate-page"
                            blockName="menu-map-control"
                        >
                            <div className="row"></div>
                        </MenuItem>
                        <MenuItem
                            title="Object editor"
                            elementName="object-editor-page"
                            blockName="menu-map-control"
                        >
                            <div className="row"></div>
                        </MenuItem>
                        <MenuItem
                            title="Layer switcher"
                            elementName="layer-switcher-page"
                            blockName="menu-map-control"
                        >
                            <div className="row">Layer switcher</div>
                        </MenuItem>
                        <MenuItem
                            title="Address search"
                            elementName="address-search-page"
                            blockName="menu-map-control"
                        >
                            <div className="row">
                                <AddressSearchForm
                                    blockName="menu-map-control"
                                    addressQuery="Test address"
                                />
                            </div>
                        </MenuItem>
                        <MenuItem
                            title="Files"
                            elementName="files-page"
                            blockName="menu-map-control"
                        >
                            <div className="row">Files</div>
                        </MenuItem>

                    </MenuMapControl>
                    <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                    <Circle center={position} fillColor="blue" radius={200}/>
                </LeafletMap>
            </div>
        );
    }
}
