import { IBlockProps } from '../../props/block-props';

export interface IKeyMobileAppProps extends IBlockProps {
    activeElementName: string;
}
