import * as React from 'react';
import {observer} from 'mobx-react';
import {ThemedBlock} from '../../../common/themed-block';
import {IBlockProps} from '../../../props/block-props';

type AddressSearchFormProps = IBlockProps & {
    addressQuery?: string;
};

@observer
export class AddressSearchForm extends ThemedBlock<AddressSearchFormProps> {
    public render(): React.ReactNode {
        return (
            <form className="col-12">
                <div className="form-group">
                    <label>
                        Address
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        value={this.props.addressQuery}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-primary"
                >
                    Submit
                </button>
            </form>
        );
    }
}
