import * as React from 'react';
import { observer } from 'mobx-react';
import { ThemedBlock } from '../../../common/themed-block';
import { IBlockProps } from '../../../props/block-props';

type DistanceMapControlProps = IBlockProps & {
    isPaused: boolean;
    distance: number;
};

@observer
export class DistanceForm extends ThemedBlock<DistanceMapControlProps> {
    public get distanceLabel(): string {
        return (this.props.distance / 1000).toLocaleString('ru-ru', { minimumFractionDigits: 3 });
    }

    public render(): React.ReactNode {
        const button = this.props.isPaused ? (
            <button
                id="distance-map-control__ok-btn"
                type="button"
                className="btn btn-light btn-outline-dark"
                title="My correct position"
                onClick={alert}
            >
                <i className="fas fa-play" />
            </button>
        ) : (
            <button
                id="distance-map-control__pause-btn"
                type="button"
                className="btn btn-light btn-outline-dark"
                title="My correct position"
            >
                <i className="fas fa-pause" />
            </button>
        );

        return (
            <div className="distance alert alert-info">
                Path distance is {this.distanceLabel} km&nbsp;
                {button}
            </div>
        );
    }
}
