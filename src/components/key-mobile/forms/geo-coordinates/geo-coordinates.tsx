import { observer } from 'mobx-react';
import React from 'react';
import ReactDOM from 'react-dom';
import { convertDDToDMS } from '../../../../utils/converter';
import { ThemedBlock } from '../../../common/themed-block';

import { IBlockProps } from '../../../props/block-props';

export type GeoCoordinatesMapControlProps = IBlockProps & {
    latitude: number;
    longitude: number;
    altitude: number;
    accuracy: number;

    isGeolocationAvailable: boolean;
    isGeolocationEnabled: boolean;
    hasGeolocationError: boolean;
};

@observer
class GeoCoordinates extends ThemedBlock<GeoCoordinatesMapControlProps> {
    public getGeolocationDisabledNode(): React.ReactNode {
        if (!this.props.isGeolocationEnabled) {
            return (
                <div className="geo-coordinate-container">
                    <span>Geolocation is disabled</span>
                </div>
            );
        }

        return null;
    }

    public getGeolocationNotSupportedNode(): React.ReactNode {
        if (!this.props.isGeolocationAvailable) {
            return (
                <div className="geo-coordinate-container">
                    <span>Your browser does not support Geolocation</span>
                </div>
            );
        }

        return null;
    }

    public get latitudeLabel(): string {
        const lat = convertDDToDMS(this.props.latitude, 'lat');

        return `${lat.deg}° ${lat.min}' ${lat.sec}'' ${lat.direction}`;
    }

    public get longitudeLabel(): string {
        const lon = convertDDToDMS(this.props.longitude, 'lng');

        return `${lon.deg}° ${lon.min}' ${lon.sec}'' ${lon.direction}`;
    }

    public get accuracyLabel(): string {
        return `${this.props.accuracy / 1000} km`;
    }

    public render(): React.ReactNode {
        if (!this.props.isGeolocationAvailable) {
            return this.getGeolocationDisabledNode();
        }

        if (!this.props.isGeolocationEnabled) {
            return this.getGeolocationNotSupportedNode();
        }

        if (!this.props.hasGeolocationError) {
            return (
                <>
                    <div className="col-3">
                        <span className="geo-coordinates-card__latitude-label">Latitude:</span>
                    </div>
                    <div className="col-9">
                        <span className="geo-coordinates-card__latitude-value">
                            {this.latitudeLabel}
                        </span>
                    </div>
                    <div className="col-3">
                        <span className="geo-coordinates-card__longitude-label">Longitude:</span>
                    </div>
                    <div className="col-9">
                        <span className="geo-coordinates-card__longitude-value">
                            {this.longitudeLabel}
                        </span>
                    </div>
                    <div className="col-3">
                        <span className="geo-coordinates-card__altitude-label">Altitude:</span>
                    </div>
                    <div className="col-9">
                        <span className="geo-coordinates-card__altitude-value">
                            {this.props.altitude}
                        </span>
                    </div>
                    <div className="col-3">
                        <span className="geo-coordinates-card__accuracy-label">Accuracy:</span>
                    </div>
                    <div className="col-9">
                        <span className="geo-coordinates-card__accuracy-value">
                            {this.accuracyLabel}
                        </span>
                    </div>
                </>
            );
        }

        return null;
    }
}

export { GeoCoordinates };
