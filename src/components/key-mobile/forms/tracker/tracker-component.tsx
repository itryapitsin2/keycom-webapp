import React from 'react';
import { observer } from 'mobx-react';
import { Polyline } from 'react-leaflet';
import { LatLngTuple } from 'leaflet';

import { ControllerComponent } from '../../../common/controller-component';
import { IBaseComponentProps } from '../../../common/map/controls/base-map-control-props';
import { TrackerController } from '../../../../controllers/key-mobile/tracker-controller';
import { IBlockControllerProps } from '../../../props/controller-props';

type TrackerComponentProps = IBaseComponentProps & IBlockControllerProps<TrackerController>;

@observer
class TrackerComponent extends ControllerComponent<TrackerComponentProps, TrackerController> {
    public render(): React.ReactNode {
        if (!this.props.isDisplay) {
            return null;
        }

        if (this.controller.trackerStore.points.length > 0) {
            const positions = this.controller.trackerStore.points.map(
                (x: LatLngTuple): LatLngTuple => [x[0], x[1]],
            );
            return <Polyline positions={positions} />;
        }

        return null;
    }
}

export { TrackerComponent };
