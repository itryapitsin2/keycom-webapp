// // @flow
//
// import L from 'leaflet';
//
// import type {
//     Map,
//     LatLng,
// } from 'leaflet';
//
// type Marker = typeof L.Marker;
// type LayerGroup = typeof L.LayerGroup;
// type Polyline = typeof L.Polyline;
// type Control = typeof L.Control;
// type Icon = typeof L.Icon;
//
// class TrackerContainer extends L.Layer {
//     that: Control;
//
//     wayPoints: LatLng[] = [];
//
//     polyline: Polyline;
//
//     marker: Marker;
//
//     layerGroup: LayerGroup;
//
//     map: Map;
//
//     icon: Icon;
//
//     constructor(that: L.Control, options: *) {
//         super(options);
//         this.that = that;
//         this.icon = L.icon({
//             iconUrl: 'leaf-green.png',
//
//             iconSize: [38, 95], // size of the icon
//             shadowSize: [50, 64], // size of the shadow
//             iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
//             shadowAnchor: [4, 62], // the same for the shadow
//             popupAnchor: [-3, -76], // point from which the popup should open relative to the
//             // iconAnchor
//         });
//     }
//
//     onAdd(map: Map): LayerGroup {
//         this.marker = L.marker(
//             map.getCenter(),
//             // { icon: this.icon },
//         );
//         this.wayPoints.push(map.getCenter());
//         this.polyline = L.polyline(this.wayPoints);
//
//         this.layerGroup = L
//             .layerGroup([this.marker, this.polyline])
//             .addTo(map);
//
//         return this.layerGroup;
//     }
//
//     setNewPosition(latlng: LatLng) {
//         this.marker.setLatLng(latlng);
//         this.wayPoints.push(latlng);
//         this.polyline.setLatLngs(this.wayPoints);
//     }
//
//     onRemove(map: Map) {
//         map.removeLayer(this.layerGroup);
//         const polygon = L.polygon([[1, 2]]);
//         polygon.toGeoJSON();
//     }
// }
//
// export default TrackerContainer;
