import React from 'react';
import { observer } from 'mobx-react';

import { IBaseMapControlProps } from '../../../common/map/controls/base-map-control-props';
import { SimpleMapControl } from '../../../common/map/controls/simple-map-control';
import { ControlContainer } from '../../../common/map/controls/control-container';
import ReactDOM from 'react-dom';

type MenuMapControlProps = IBaseMapControlProps & {};

@observer
class MenuMapControl extends SimpleMapControl<ControlContainer, MenuMapControlProps> {
    public createContainer(): ControlContainer {
        return new ControlContainer('menu-map-control-container');
    }

    public render(): React.ReactNode {
        if (!this.leafletElement || !this.leafletElement.getContainer()) {
            return null;
        }

        const menu = (
            <div className="pos-f-t menu-map-control shadow-sm">
                <div className="collapse" id="navbarToggleExternalContent">
                    <div className="bg-light">
                        <div className="accordion" id="menu-map-control">
                            {this.props.children}
                        </div>
                    </div>
                </div>
                <nav className="navbar navbar-light bg-light">
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarToggleExternalContent"
                        aria-controls="navbarToggleExternalContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon" />
                    </button>
                </nav>
            </div>
        );

        return ReactDOM.createPortal(menu, this.leafletElement.getContainer());
    }
}

export { MenuMapControl };
