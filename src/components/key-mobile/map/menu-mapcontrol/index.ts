import { withLeaflet } from 'react-leaflet';
import { MenuMapControl } from './component';

export default withLeaflet(MenuMapControl);
