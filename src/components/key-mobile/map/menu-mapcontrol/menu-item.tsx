import { ThemedElement } from '../../../common/themed-element';
import { IElementProps } from '../../../props/element-props';
import React from 'react';

type PageComponentPros = IElementProps & {
    title?: string;
};

export class MenuItem extends ThemedElement<PageComponentPros> {
    public render(): React.ReactNode {
        return (
            <div className="card">
                <div className="card-header">
                    <h2 className="mb-0">
                        <button
                            className="btn btn-link"
                            type="button"
                            data-toggle="collapse"
                            data-target={`#${this.props.elementName}`}
                            aria-controls={this.props.elementName}
                        >
                            {this.props.title}
                        </button>
                    </h2>
                </div>
                <div
                    id={this.props.elementName}
                    className="collapse"
                    aria-labelledby={this.props.elementName}
                    data-parent="#menu-map-control"
                >
                    <div className="card-body">{this.props.children}</div>
                </div>
            </div>
        );
    }
}
