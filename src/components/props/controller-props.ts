import { IBlockProps } from './block-props';

export interface IBlockControllerProps<T> extends IBlockProps, IControllerProps<T> {}

export interface IControllerProps<T> {
    controller: T;
}
