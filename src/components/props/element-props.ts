import { IBlockProps } from './block-props';

export interface IElementProps extends IBlockProps {
    elementName: string;
}

export interface IActionElementProps<T> extends IElementProps {
    action: (e: T) => void;
}
