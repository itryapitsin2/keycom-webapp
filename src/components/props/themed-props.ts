import { IClassNameProps } from '@bem-react/core';

export interface IThemedProps extends IClassNameProps {
    theme?: string;
}
