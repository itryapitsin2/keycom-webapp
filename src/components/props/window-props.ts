import { IElementProps } from './element-props';

export interface IWindowProps extends IElementProps {
    draggableHandler?: string;
}
