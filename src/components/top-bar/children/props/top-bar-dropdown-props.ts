import * as React from 'react';
import { IThemedProps } from '../../../props/themed-props';

export interface ITopBarDropdownProps extends IThemedProps {
    float?: string;
    icon?: React.ReactNode;
}
