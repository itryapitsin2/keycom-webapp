import * as React from 'react';
import { ITopBarButtonProps } from './props/top-bar-button-props';
import { ThemedBlock } from '../../common/themed-block';
import { ThemedElement } from '../../common/themed-element';

class TopBarButton<T extends ITopBarButtonProps = ITopBarButtonProps> extends ThemedElement<T> {
    public render(): any {
        return (
            <button type="button" className={this.elementClassName} onClick={this.props.action}>
                {this.props.children}
            </button>
        );
    }
}

export { TopBarButton };
