// import * as React from "react";
// import {className} from "../../../utils";
//
// import {ThemedBlock} from "../../themed-block";
// import {BlockName} from "../constants";
// import {ClassNameFormatter} from "@bem-react/classname";
// import {ITopBarDropdownProps} from "./props/top-bar-dropdown-props";
//
// export const ElementName = "dropdown";
// const cnTopBarDropdown = className(BlockName, ElementName);
//
// class TopBarDropdown<T extends ITopBarDropdownProps = ITopBarDropdownProps> extends ThemedBlock<T> {
//
//     protected className: string;
//     protected classNameFormatter: ClassNameFormatter = cnTopBarDropdown;
//
//     public constructor(props: Readonly<T>) {
//         super(props);
//
//         const theme = props.theme;
//         this.className = this.classNameFormatter(
//             {theme},
//             []
//         );
//     }
//
//     public render(): any {
//         return <div className={this.className}>
//             <button type="button" className="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
//                     aria-expanded="false" title="KeyCom Tools">
//                 {this.props.icon}
//             </button>
//             <div className="dropdown-menu">
//                 {this.props.children}
//             </div>
//         </div>
//     }
// }
//
// export {TopBarDropdown}
