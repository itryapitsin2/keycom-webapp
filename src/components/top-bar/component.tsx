import React from 'react';

import { ITopBarProps } from './props/top-bar-props';
import { ThemedBlock } from '../common/themed-block';

class TopBar extends ThemedBlock<ITopBarProps> {
    public render(): React.ReactNode {
        return (
            <div className={this.blockClassName}>
                <form className="form-inline">{this.props.children}</form>
            </div>
        );
    }
}

export { TopBar };
