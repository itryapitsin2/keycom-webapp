import React, { PropsWithChildren } from 'react';

import { ThemedElement } from '../common/themed-element';
import { IActionElementProps, IElementProps } from '../props/element-props';
import { IWindowProps } from '../props/window-props';
import Draggable from 'react-draggable';
import { IWindowStates } from '../states/window-states';

export class WindowHeader extends ThemedElement<IElementProps> {
    public render(): React.ReactNode {
        return (
            <div className={`${this.elementClassName} keypro--modal__header`} role="dialog">
                {this.props.children}
            </div>
        );
    }
}

export class WindowHeaderButtonsContainer extends React.PureComponent<PropsWithChildren<{}>> {
    public render(): React.ReactNode {
        return <div className="keypro--modal__header-btn-container">{this.props.children}</div>;
    }
}

export class WindowHeaderMinimize extends ThemedElement<IActionElementProps<{}>> {
    public render(): React.ReactNode {
        return (
            <button
                type="button"
                className={`${this.elementClassName} keypro--modal__minimize`}
                data-dismiss="modal"
                aria-label="Minimize"
                onClick={this.props.action}
            >
                <i className="fas fa-window-minimize" />
            </button>
        );
    }
}

export class WindowHeaderFavorite extends ThemedElement<IActionElementProps<{}>> {
    public render(): React.ReactNode {
        return (
            <button
                type="button"
                className={`${this.elementClassName} keypro--modal__favorite`}
                data-dismiss="modal"
                aria-label="Favorite"
                onClick={this.props.action}
            >
                <i className="fas fa-star" />
            </button>
        );
    }
}

export class WindowHeaderClose extends ThemedElement<IActionElementProps<{}>> {
    public render(): React.ReactNode {
        return (
            <button
                type="button"
                className={`${this.elementClassName} keypro--modal__close`}
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.action}
            >
                <span aria-hidden="true">&times;</span>
            </button>
        );
    }
}

export class WindowBody extends ThemedElement<IElementProps> {
    public render(): React.ReactNode {
        return (
            <div className={`${this.elementClassName} keypro--modal__body`} role="dialog">
                {this.props.children}
            </div>
        );
    }
}

export class WindowFooter extends ThemedElement<IElementProps> {
    public render(): React.ReactNode {
        return (
            <div className={`${this.elementClassName} keypro--modal__footer`} role="dialog">
                {this.props.children}
            </div>
        );
    }
}

export class Window extends ThemedElement<IWindowProps, IWindowStates> {
    protected readonly minZIndex: number = 19000;
    protected readonly maxZIndex: number = 50000;

    private wrapperRef: HTMLDivElement;

    public constructor(props: IWindowProps) {
        super(props);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.setWrapperRef = this.setWrapperRef.bind(this);
    }

    private setZIndex = (zIndex: number): void => {
        this.setState({ zIndex });
    };

    public componentDidMount(): void {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    public componentWillUnmount(): void {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    protected handleClickOutside(event: MouseEvent & { target: HTMLElement }): void {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setZIndex(this.minZIndex);
        }
    }

    private setWrapperRef(node: HTMLDivElement): void {
        this.wrapperRef = node;
    }

    public render(): React.ReactNode {
        const zIndex = this.state ? this.state.zIndex : this.maxZIndex;

        return (
            <Draggable handle={`.${this.getCustomElementClassName(this.props.draggableHandler)}`}>
                <div className="draggable-container" style={{ zIndex }}>
                    <div
                        role="dialog"
                        className={`${this.elementClassName} keypro--modal`}
                        ref={this.setWrapperRef}
                        onMouseDown={(): void => this.setZIndex(this.maxZIndex)}
                    >
                        <div className="keypro--modal__dialog" role="document">
                            <div className="keypro--modal__content">{this.props.children}</div>
                        </div>
                    </div>
                </div>
            </Draggable>
        );
    }
}
