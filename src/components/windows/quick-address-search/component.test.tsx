import React from 'react';
import { mount } from 'enzyme';

import { QuickAddressSearchWindow } from './component';
import { Container } from 'inversify';
import { StoreModule } from '../../../stores/module';
import { ControllerModule } from '../../../controllers/module';
import { QuickAddressSearchWindowController } from '../../../controllers/windows/quick-address-search-window';
import { MockApiModule } from '../../../api/mock-module';

it('QuickAddressSearchWindow testing', async (done): Promise<void> => {
    const di = new Container();
    di.load(StoreModule, MockApiModule, ControllerModule);
    const quickAddressSearchWindowController = di.get(QuickAddressSearchWindowController);
    quickAddressSearchWindowController.quickAddressSearchWindowStore.show();

    const wrapper = mount(
        <QuickAddressSearchWindow
            blockName="quick-address-search"
            controller={quickAddressSearchWindowController}
        />,
    );
    const searchInput = wrapper.find('.keypro--quick-address-search__search-input');

    searchInput.instance().value = 'Test';
    searchInput.simulate('change');

    expect(quickAddressSearchWindowController.quickAddressSearchWindowStore.isLoading).toBeTruthy();

    setImmediate((): void => {
        wrapper.update();

        const ddm = wrapper.find('.dropdown-menu');
        expect(ddm).toHaveLength(1); // dropdown visible

        expect(
            quickAddressSearchWindowController.quickAddressSearchWindowStore.isLoading,
        ).toBeFalsy();
        expect(
            quickAddressSearchWindowController.quickAddressSearchService.getAddressesByPattern,
        ).toBeCalledTimes(1);
        expect(quickAddressSearchWindowController.quickAddressSearchWindowStore.items).toHaveLength(
            1,
        );
        done();
    });
});
