import React from 'react';
import { ClassNameFormatter } from '@bem-react/classname';
import { observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import { IQuickAddressSearchWindowProps } from './props';
import { ControllerComponent } from '../../common/controller-component';
import { QuickAddressSearchWindowController } from '../../../controllers/windows/quick-address-search-window';
import {
    Window,
    WindowBody,
    WindowFooter,
    WindowHeader,
    WindowHeaderButtonsContainer,
    WindowHeaderClose,
    WindowHeaderFavorite,
    WindowHeaderMinimize,
} from '../base-window';
import { className } from '../../../utils';
import { Address } from '../../../api/models/address';

/**
 *
 *
 * @class Component
 * @extends {ControllerComponent<IQuickAddressSearchWindowProps, QuickAddressSearchWindowController>}
 */
@observer
class Component extends ControllerComponent<
    IQuickAddressSearchWindowProps,
    QuickAddressSearchWindowController
> {
    private readonly searchInputFormatter: ClassNameFormatter;
    public readonly locateBtnFormatter: ClassNameFormatter;
    private readonly addressesDropdownFormatter: ClassNameFormatter;
    private readonly titleFormatter: ClassNameFormatter;
    private readonly closeBtnFormatter: ClassNameFormatter;

    private readonly titleDialogBlockName = 'quick-search-title-dialog';
    private readonly titleDialogTitleFormatter: ClassNameFormatter;
    private readonly titleDialogLabelFormatter: ClassNameFormatter;
    private readonly titleDialogTitleInputFormatter: ClassNameFormatter;
    private readonly titleDialogOkFormatter: ClassNameFormatter;
    private readonly titleDialogCancelFormatter: ClassNameFormatter;

    public constructor(props: IQuickAddressSearchWindowProps) {
        super(props);

        this.titleFormatter = className(this.blockName, 'modal-title');
        this.closeBtnFormatter = className(this.blockName, 'close');

        this.searchInputFormatter = className(this.blockName, 'search-input');
        this.locateBtnFormatter = className(this.blockName, 'locate-btn');
        this.addressesDropdownFormatter = className(this.blockName, 'addresses-dd');

        this.titleDialogTitleFormatter = className(this.titleDialogBlockName, 'modal-title');
        this.titleDialogLabelFormatter = className(this.titleDialogBlockName, 'title-label');
        this.titleDialogTitleInputFormatter = className(this.titleDialogBlockName, 'title-input');
        this.titleDialogOkFormatter = className(this.titleDialogBlockName, 'ok-btn');
        this.titleDialogCancelFormatter = className(this.titleDialogBlockName, 'cancel-btn');
    }

    private get titleDialog(): React.ReactNode {
        const theme = this.theme;

        return this.controller.quickAddressSearchWindowStore.isShowTitleDialog ? (
            <Window
                blockName={this.titleDialogBlockName}
                elementName="title-dialog"
                draggableHandler="header"
            >
                <WindowHeader blockName={this.titleDialogBlockName} elementName="header">
                    <h5 className={this.titleDialogTitleFormatter({ theme })}>
                        {this.props.t('Saving window on The Favorite Bar...')}
                    </h5>
                    <WindowHeaderButtonsContainer>
                        <WindowHeaderClose
                            blockName={this.titleDialogBlockName}
                            className="keypro--modal__red"
                            elementName="close"
                            action={this.controller.quickAddressSearchWindowStore.hideTitleDialog}
                        />
                    </WindowHeaderButtonsContainer>
                </WindowHeader>
                <WindowBody blockName={this.titleDialogBlockName} elementName="body">
                    <form>
                        <div className="form-group row">
                            <label htmlFor="title" className={this.titleDialogLabelFormatter()}>
                                {this.props.t('Title')}:
                            </label>
                            <div className="col-sm-10">
                                <input
                                    type="text"
                                    className={`${this.titleDialogTitleInputFormatter()} form-control`}
                                    placeholder={this.props.t(
                                        'Input title for the element in the favorite bar',
                                    )}
                                    value={
                                        this.controller.quickAddressSearchWindowStore.favWindowTitle
                                    }
                                    onChange={this.controller.onSetFavWindowTitle}
                                />
                            </div>
                        </div>
                    </form>
                </WindowBody>
                <WindowFooter blockName={this.titleDialogBlockName} elementName="footer">
                    <button
                        className={this.titleDialogOkFormatter()}
                        type="button"
                        onClick={this.controller.onOkDialogTitle}
                    >
                        <i className="fas fa-check" />
                        &nbsp;
                        <span>{this.props.t('Ok')}</span>
                    </button>
                    <button
                        className={this.titleDialogCancelFormatter()}
                        type="button"
                        onClick={this.controller.onCancelDialogTitle}
                    >
                        <i className="fas fa-times" />
                        &nbsp;
                        <span>{this.props.t('Cancel')}</span>
                    </button>
                </WindowFooter>
            </Window>
        ) : null;
    }

    public render(): React.ReactNode {
        if (!this.controller.quickAddressSearchWindowStore.isShow) {
            return null;
        }

        const theme = this.theme;

        // TODO: replace more useful dropdown
        const items = this.controller.quickAddressSearchWindowStore.items.map(
            (item: Address): React.ReactNode => (
                <span
                    key={item.id}
                    style={{ cursor: 'pointer' }}
                    data-item={item.name}
                    className="dropdown-item"
                    onClick={this.controller.onSelectItem}
                >
                    {item.name}
                </span>
            ),
        );
        const resultsDropdown =
            this.controller.quickAddressSearchWindowStore.items.length !== 0 ? (
                <div className="dropdown-menu" style={{ display: 'unset', width: '100%' }}>
                    {items}
                </div>
            ) : null;

        return (
            <div className={this.blockName}>
                <Window blockName={this.blockName} elementName="dialog" draggableHandler="header">
                    <WindowHeader blockName={this.blockName} elementName="header">
                        <h5 className={this.titleFormatter({ theme })}>
                            {this.props.t('Quick address search')}
                        </h5>
                        <WindowHeaderButtonsContainer>
                            <WindowHeaderMinimize
                                blockName={this.blockName}
                                className="keypro--modal__default"
                                elementName="minimize"
                                action={this.controller.onMinimize}
                            />
                            <WindowHeaderFavorite
                                blockName={this.blockName}
                                className="keypro--modal__green"
                                elementName="favorite"
                                action={
                                    this.controller.quickAddressSearchWindowStore.showTitleDialog
                                }
                            />
                            <WindowHeaderClose
                                blockName={this.blockName}
                                className="keypro--modal__red"
                                elementName="close"
                                action={this.controller.onClose}
                            />
                        </WindowHeaderButtonsContainer>
                    </WindowHeader>
                    <WindowBody blockName={this.blockName} elementName="body">
                        <div className="row">
                            <div className="col-12">
                                <div className="input-group">
                                    <input
                                        type="text"
                                        className={`${this.searchInputFormatter()} form-control`}
                                        placeholder={this.props.t('Input address')}
                                        value={
                                            this.controller.quickAddressSearchWindowStore.pattern
                                        }
                                        onChange={this.controller.onSetAddressesByPattern}
                                    />
                                    {resultsDropdown}
                                    <div className="input-group-append">
                                        <button
                                            className={`${this.locateBtnFormatter()} btn btn-outline-secondary`}
                                            type="button"
                                            onClick={this.controller.onLocateToPoint}
                                        >
                                            <i className="fas fa-search-location" />
                                        </button>
                                        <button className="btn btn-outline-secondary" type="button">
                                            <i className="fas fa-binoculars" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </WindowBody>
                </Window>
                {this.titleDialog}
            </div>
        );
    }
}

export const QuickAddressSearchWindow = withTranslation()(Component);
