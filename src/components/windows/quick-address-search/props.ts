import * as React from 'react';
import { IWindowProps } from '../props';
import { IBlockControllerProps } from '../../props/controller-props';
import { QuickAddressSearchWindowController } from '../../../controllers/windows/quick-address-search-window';
import { WithTranslation } from 'react-i18next';

export interface IQuickAddressSearchWindowProps
    extends IWindowProps,
        IBlockControllerProps<QuickAddressSearchWindowController>,
        WithTranslation {}
