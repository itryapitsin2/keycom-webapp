import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { FavWindowElementStore } from '../stores/footer/fav-window-element-store';
import { FavWindowElement } from '../stores/footer/fav-window-element';
import { IFavWindowService } from '../api/services/fav-window-service';

@injectable()
export class FooterBarController {
    private readonly _favWindowElementStore: FavWindowElementStore;

    public constructor(
        @inject(FavWindowElementStore) favWindowElementStore,
        @inject('FavWindowService') favWindowService: IFavWindowService,
    ) {
        this._favWindowElementStore = favWindowElementStore;
    }

    public get favWindowElementStore(): FavWindowElementStore {
        return this._favWindowElementStore;
    }

    public onDeleteElement = (element: FavWindowElement): void => {
        this._favWindowElementStore.elements.forEach((value, index) => {
            if (value === element) {
                this._favWindowElementStore.elements.splice(index, 1);
            }
        });
    };
}
