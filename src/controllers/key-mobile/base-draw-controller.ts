import { inject, postConstruct } from 'inversify';
import L, { LeafletMouseEvent } from 'leaflet';
import { DrawingStore } from '../../stores/drawing-store';
import { Line } from '../../stores/geo-objects/line';

class BaseDrawController {
    public drawingStore: DrawingStore;

    public constructor(@inject(DrawingStore) drawingStore) {
        this.drawingStore = drawingStore;
    }

    @postConstruct()
    public onInit(): void {
        this.addOrUpdatePointPosition = this.addOrUpdatePointPosition.bind(this);
        this.refreshLayerGroup = this.refreshLayerGroup.bind(this);
    }

    protected refreshLayerGroup(layerGroup: L.LayerGroup): void {
        if (this.drawingStore.object.leafletObject) {
            layerGroup.clearLayers();
            layerGroup.addLayer(this.drawingStore.object.leafletObject);
        }
    }

    public lastPointDynamicUpdate(event: LeafletMouseEvent, layerGroup: L.LayerGroup): void {
        if (this.drawingStore.object instanceof Line) {
            const line: Line = this.drawingStore.object;
            line.dynamicLastPoint = event.latlng;
            this.refreshLayerGroup(layerGroup);
        }
    }

    public addOrUpdatePointPosition(event: LeafletMouseEvent, layerGroup: L.LayerGroup): void {
        if (this.drawingStore.object) {
            this.drawingStore.object.setLastPoint(event.latlng);
            this.refreshLayerGroup(layerGroup);
        }
    }
}

export { BaseDrawController };
