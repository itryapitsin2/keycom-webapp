import L, { LeafletMouseEvent } from 'leaflet';
import { action, computed } from 'mobx';

import { Line } from '../../stores/geo-objects/line';
import { postConstruct } from 'inversify';
import { DrawMapControlController } from './draw-map-control-controller';

export class DistanceMapControlController extends DrawMapControlController {
    @postConstruct()
    public onInit(): void {
        super.onInit();
        this.startDrawing = this.startDrawing.bind(this);
        this.pauseDrawing = this.pauseDrawing.bind(this);
        this.addOrUpdatePointPosition = this.addOrUpdatePointPosition.bind(this);
        this.refreshLayerGroup = this.refreshLayerGroup.bind(this);
    }

    @action
    public startDrawing(layerGroup: L.LayerGroup): void {
        this.drawingStore.object = new Line();
        this.drawingStore.onPause = false;
        super.refreshLayerGroup(layerGroup);
    }

    @action
    public pauseDrawing(layerGroup: L.LayerGroup): void {
        this.drawingStore.onPause = true;
        if (this.drawingStore.object instanceof Line) {
            const line: Line = this.drawingStore.object;
            line.dynamicLastPoint = null;
            super.refreshLayerGroup(layerGroup);
        }
    }

    public lastPointDynamicUpdate(event: LeafletMouseEvent, layerGroup: L.LayerGroup): void {
        if (!this.drawingStore.onPause) {
            super.lastPointDynamicUpdate(event, layerGroup);
        }
    }

    public addOrUpdatePointPosition(event: LeafletMouseEvent, layerGroup: L.LayerGroup): void {
        if (!this.drawingStore.onPause) {
            super.addOrUpdatePointPosition(event, layerGroup);
        }
    }

    public distanceLabel(): string {
        let distance = 0;
        if (this.drawingStore.object instanceof Line) {
            const line: Line = this.drawingStore.object;
            distance = line.length;
        }

        return (distance / 1000).toLocaleString('ru-ru', { minimumFractionDigits: 3 });
    }

    @computed
    public get isPaused(): boolean {
        return this.drawingStore.onPause;
    }
}
