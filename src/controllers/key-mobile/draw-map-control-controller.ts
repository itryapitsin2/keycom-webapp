import { postConstruct } from 'inversify';

import { BaseDrawController } from './base-draw-controller';
import { NotImplemented } from '../../utils/exceptions';

class DrawMapControlController extends BaseDrawController {
    @postConstruct()
    public onInit(): void {
        super.onInit();
        this.ok = this.ok.bind(this);
    }

    public ok: VoidFunction = (): void => {
        throw new NotImplemented('DrawMapControlViewModel#ok');
    };
}

export { DrawMapControlController };
