import { injectable } from 'inversify';
import { TrackerStore } from '../../stores/key-mobile/tracker-store';

/**
 * Contains geo-position tracker logic
 */
@injectable()
export class TrackerController {
    public trackerStore: TrackerStore;

    public constructor(trackerStore: TrackerStore) {
        this.trackerStore = trackerStore;
    }
}
