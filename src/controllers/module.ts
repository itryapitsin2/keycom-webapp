import { ContainerModule, interfaces } from 'inversify';
import { QuickAddressSearchWindowController } from './windows/quick-address-search-window';
import { TopBarController } from './top-bar';
import { FooterBarController } from './footer-bar';

export const ControllerModule = new ContainerModule((bind: interfaces.Bind): void => {
    bind(QuickAddressSearchWindowController)
        .toSelf()
        .inSingletonScope();

    bind(TopBarController)
        .toSelf()
        .inSingletonScope();

    bind(FooterBarController)
        .toSelf()
        .inSingletonScope();

    // aliases for JS
    bind('QuickAddressSearchWindowController').to(QuickAddressSearchWindowController);
    bind('TopBarController').to(TopBarController);
});
