import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { QuickAddressSearchWindowStore } from '../stores/windows/quick-address-search-window-store';

@injectable()
class TopBarController {
    private quickAddressSearchWindowStore: QuickAddressSearchWindowStore;

    public constructor(
        @inject(QuickAddressSearchWindowStore)
        quickAddressSearchWindowStore: QuickAddressSearchWindowStore,
    ) {
        this.quickAddressSearchWindowStore = quickAddressSearchWindowStore;
    }

    public logout = (): void => {
        console.log('Called TopBarService#logout method');
    };

    public showAddress = (): void => {};

    public showAnnotations = (): void => {};

    public showArea = (): void => {};

    public showDiagram = (): void => {};

    public showExternalDocuments = (): void => {};

    public showLine = (): void => {};

    public showPlan = (): void => {};

    public showProfile = (): void => {};

    public showQuickAddressSearch = (): void => {
        if (typeof keygwt !== 'undefined' && keygwt !== null) {
            keygwt.forms.keygwt_quickAddressSearch.open();
            console.log('Called TopBarService#showQuickAddressSearch method');
        } else {
            console.log('Keygwt in not available');
        }
    };

    public showSpatialUpload = (): void => {};

    public showSurveyFileImport(): void {}

    public showText = (): void => {};

    public showHelloWorld = (): void => {
        this.quickAddressSearchWindowStore.show();
    };
}

export { TopBarController };
