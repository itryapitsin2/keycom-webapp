import React from 'react';
import { Container } from 'inversify';
import { StoreModule } from '../../stores/module';
import { MockApiModule } from '../../api/mock-module';
import { ControllerModule } from '../module';
import { QuickAddressSearchWindowController } from './quick-address-search-window';

it('QuickAddressSearchWindowController testing', async (done): Promise<void> => {
    const di = new Container();
    di.load(StoreModule, MockApiModule, ControllerModule);
    const quickAddressSearchWindowController = di.get(QuickAddressSearchWindowController);
    quickAddressSearchWindowController.quickAddressSearchWindowStore.show();

    const event = { currentTarget: { value: 'Test' } } as React.FormEvent<HTMLInputElement>;
    quickAddressSearchWindowController.loadAddressesByPattern(event);

    expect(quickAddressSearchWindowController.quickAddressSearchWindowStore.isLoading).toBeTruthy();

    setImmediate((): void => {
        expect(
            quickAddressSearchWindowController.quickAddressSearchWindowStore.isLoading,
        ).toBeFalsy();
        expect(
            quickAddressSearchWindowController.quickAddressSearchService.getAddressesByPattern,
        ).toBeCalledTimes(1);
        expect(quickAddressSearchWindowController.quickAddressSearchWindowStore.items).toHaveLength(
            1,
        );

        done();
    });
});
