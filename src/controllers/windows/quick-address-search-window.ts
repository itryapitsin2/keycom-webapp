import * as React from 'react';
import { inject, injectable } from 'inversify';
import { IQuickAddressSearchService } from '../../api/services/quick-address-search-service';
import { QuickAddressSearchWindowStore } from '../../stores/windows/quick-address-search-window-store';
import { Address } from '../../api/models/address';
import { MINIMUM_CHARS_FOR_SEARCHING } from '../../utils/settings';
import { FavWindowElementStore } from '../../stores/footer/fav-window-element-store';
import { FavWindowElement } from '../../stores/footer/fav-window-element';

@injectable()
export class QuickAddressSearchWindowController {
    private readonly _quickAddressSearchService: IQuickAddressSearchService;
    private readonly _quickAddressSearchWindowStore: QuickAddressSearchWindowStore;
    private readonly _favWindowElementStore: FavWindowElementStore;

    public constructor(
        @inject('QuickAddressSearchService') quickAddressSearchService: IQuickAddressSearchService,
        @inject(QuickAddressSearchWindowStore)
        quickAddressSearchWindowStore: QuickAddressSearchWindowStore,
        @inject(FavWindowElementStore) favWindowElementStore: FavWindowElementStore,
    ) {
        this._quickAddressSearchService = quickAddressSearchService;
        this._quickAddressSearchWindowStore = quickAddressSearchWindowStore;
        this._favWindowElementStore = favWindowElementStore;
    }

    public get quickAddressSearchService(): IQuickAddressSearchService {
        return this._quickAddressSearchService;
    }

    public get quickAddressSearchWindowStore(): QuickAddressSearchWindowStore {
        return this._quickAddressSearchWindowStore;
    }

    public onSelectItem = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
        const item = this.quickAddressSearchWindowStore.items.find(
            (x: Address): boolean =>
                x.name === e.currentTarget.attributes.getNamedItem('data-item').value,
        );

        this.quickAddressSearchWindowStore.selectItem(item);
        this.quickAddressSearchWindowStore.setItems([]);
    };

    public onSetAddressesByPattern = async (
        e: React.FormEvent<HTMLInputElement>,
    ): Promise<void> => {
        const pattern = e.currentTarget.value;
        this.quickAddressSearchWindowStore.setPattern(pattern);
        this.tryLoadAddressesByPattern(pattern);
    };

    public onSetFavWindowTitle = async (e: React.FormEvent<HTMLInputElement>): Promise<void> => {
        const pattern = e.currentTarget.value;
        this.quickAddressSearchWindowStore.setFavWindowTitle(pattern);
        // check that it is unique
    };

    public onLocateToPoint = (): void => {
        if (!this.quickAddressSearchWindowStore.selectedItem) {
            return;
        }

        this.quickAddressSearchService.locateToPoint(
            this.quickAddressSearchWindowStore.selectedItem.lat,
            this.quickAddressSearchWindowStore.selectedItem.lon,
        );
    };

    public onMinimize = (): void => {};

    public onClose = () => {
        this.onCancelDialogTitle();
        this.quickAddressSearchWindowStore.hide();
        this.quickAddressSearchWindowStore.setPattern('');
    };

    public onCancelDialogTitle = () => {
        this.quickAddressSearchWindowStore.hideTitleDialog();
        this.quickAddressSearchWindowStore.setFavWindowTitle('');
    };

    public onOkDialogTitle = () => {
        this.doFavorite(this.quickAddressSearchWindowStore.favWindowTitle);
        this.onCancelDialogTitle();
    };

    private doFavorite = (title: string): void => {
        const stateCopy = this.quickAddressSearchWindowStore.serialize();
        let favElement = this._favWindowElementStore.getElementByKey('quick-address-search');
        if (!favElement) {
            favElement = new FavWindowElement(
                'quick-address-search',
                async stateCopy => {
                    this.quickAddressSearchWindowStore.deserialize(stateCopy);
                    await this.tryLoadAddressesByPattern(
                        this.quickAddressSearchWindowStore.pattern,
                    );
                    this.quickAddressSearchWindowStore.setFavWindowTitle('');
                },
                [
                    {
                        title: title,
                        state: stateCopy,
                    },
                ],
                'fab fa-sketch',
            );
            this._favWindowElementStore.setElement(favElement);
        } else {
            favElement.params.push({ title: title, state: stateCopy });
        }
    };

    private tryLoadAddressesByPattern = async (pattern: string): Promise<void> => {
        if (pattern.length < MINIMUM_CHARS_FOR_SEARCHING) {
            return;
        }

        this._quickAddressSearchWindowStore.startLoading();

        const data = await this._quickAddressSearchService
            .getAddressesByPattern(pattern)
            .finally(() => {
                this._quickAddressSearchWindowStore.stopLoading();
            });
        this._quickAddressSearchWindowStore.setItems(data.items);
    };
}
