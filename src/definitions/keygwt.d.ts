/* eslint-disable */
///<reference path="./open-layers-2.d.ts"/>

declare function gettext(id: string): string;

declare namespace keygwt {
    namespace forms {
        namespace keygwt_quickAddressSearch {
            export function open(): void;
        }
    }

    export class Map {}

    namespace mapengine {
        export const map: Map;
        export function zoomToGeometryWithScale(
            map: Map,
            geometry: OpenLayers.Geometry,
            zoom: any,
        ): boolean;
        export function highlightFeature(feature: OpenLayers.Feature.Vector): void;
        export function systemMessage(msg: string): void;
    }
}
