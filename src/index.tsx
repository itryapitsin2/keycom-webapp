import './main.scss';
import './utils/action-logger';
import './i18n';

import launch from './launcher/launch';

launch();
