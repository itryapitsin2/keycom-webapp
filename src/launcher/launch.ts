import { PortalEmbeddedApp } from './portal-embedded-app';
import { SelfHostApp } from './self-host-app';
import { MobileApp } from './mobile-app';

export function isMobileApp(): boolean {
    const urlSearchParams = new URLSearchParams(window.location.search);
    return urlSearchParams.get('view') === 'mobile';
}

export function isPortalApp(): boolean {
    return document.body.classList.contains('keygwt');
}

export default (): void => {
    if (isMobileApp()) {
        MobileApp();
        return;
    }

    if (isPortalApp()) {
        PortalEmbeddedApp();
        return;
    }

    SelfHostApp();
};
