import * as React from 'react';
import { Container } from 'inversify';
import * as ReactDOM from 'react-dom';

import { StoreModule } from '../stores/module';
import { ApiModule } from '../api/module';
import { ControllerModule } from '../controllers/module';
import { ILauncher } from './launcher';
import { FallbackLoader } from '../components/common/fallback-loader';
import { KeyMobileApp } from '../components/key-mobile/app/app';

export const MobileApp: ILauncher = (): void => {
    const container = new Container();
    container.load(StoreModule, ApiModule, ControllerModule);

    ReactDOM.render(
        <React.Suspense fallback={<FallbackLoader />}>
            <KeyMobileApp blockName="key-mobile-app" activeElementName="root-page" />
        </React.Suspense>,
        document.querySelector('#app'),
    );
};
