import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Container } from 'inversify';

import { TopBarController } from '../controllers/top-bar';
import { QuickAddressSearchWindowController } from '../controllers/windows/quick-address-search-window';
import { TopBar as BaseTopBar, TopBarButton } from '../components/top-bar';
import { QuickAddressSearchWindow } from '../components/windows/quick-address-search/component';
import { StoreModule } from '../stores/module';
import { ApiModule } from '../api/module';
import { ControllerModule } from '../controllers/module';
import { ILauncher } from './launcher';

const Loader = () => (
    <div className="app">
        <div>loading...</div>
    </div>
);

export const PortalEmbeddedApp: ILauncher = (): void => {
    const container = new Container();
    container.load(StoreModule, ApiModule, ControllerModule);

    // Pass container to get access to the new API from old code
    // TODO: create proxy with available API
    // @ts-ignore
    window.container = container;

    const topBarController = container.get(TopBarController);
    const quickAddressSearchWindowController = container.get(QuickAddressSearchWindowController);

    ReactDOM.render(
        <BaseTopBar blockName="top-bar">
            <TopBarButton
                blockName="top-bar"
                elementName="hello-world-btn"
                action={topBarController.showHelloWorld}
            >
                <i className="far fa-comment-alt" />
            </TopBarButton>
            <TopBarButton
                blockName="top-bar"
                elementName="hello-world-btn"
                action={topBarController.showQuickAddressSearch}
            >
                <i className="far fa-lightbulb" />
            </TopBarButton>
        </BaseTopBar>,
        document.querySelector('#react-topbar-node'),
    );

    ReactDOM.render(
        <React.Suspense fallback={<Loader />}>
            <div className="modals">
                <QuickAddressSearchWindow
                    blockName="quick-address-search"
                    controller={quickAddressSearchWindowController}
                />
            </div>
        </React.Suspense>,
        document.querySelector('#react-modals-node'),
    );
};
