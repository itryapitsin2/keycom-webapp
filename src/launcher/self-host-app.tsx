import * as React from 'react';
import { Container } from 'inversify';
import * as ReactDOM from 'react-dom';

import { TopBarController } from '../controllers/top-bar';
import { QuickAddressSearchWindowController } from '../controllers/windows/quick-address-search-window';
import { StoreModule } from '../stores/module';
import { ApiModule } from '../api/module';
import { ControllerModule } from '../controllers/module';
import { ILauncher } from './launcher';
import { App } from '../components/app/app';
import { FooterBarController } from '../controllers/footer-bar';

// loading component for suspense fallback
const Loader: React.FC<{}> = (): React.ReactElement => (
    <div className="app">
        <div>loading...</div>
    </div>
);

export const SelfHostApp: ILauncher = (): void => {
    const container = new Container();
    container.bind<Container>('Container').toConstantValue(container);
    container.load(StoreModule, ApiModule, ControllerModule);

    const topBarController = container.get(TopBarController);
    const quickAddressSearchWindowController = container.get(QuickAddressSearchWindowController);
    const footerBarController = container.get(FooterBarController);

    footerBarController.favWindowElementStore.setElements([
        // new FavWindowElement((params: any[]) => {
        //         quickAddressSearchWindowController.quickAddressSearchWindowStore.show();
        //     },
        // ),
        // new FavWindowElementWithParams((params: any[]) => {
        //         alert(params)
        //     }, ['a', 'b'], <i className="fab fa-sketch"/>
        // )
    ]);

    ReactDOM.render(
        <React.Suspense fallback={<Loader />}>
            <App
                quickAddressSearchWindowController={quickAddressSearchWindowController}
                topBarController={topBarController}
                footerBarController={footerBarController}
            />
        </React.Suspense>,
        document.querySelector('#app'),
    );
};
