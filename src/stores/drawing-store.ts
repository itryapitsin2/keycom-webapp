import { computed, observable } from 'mobx';
import { injectable } from 'inversify';
import { MapObject } from './geo-objects/map-object';

export type DRAWING_MODE = 'NONE' | 'DISTANCE_CALCULATOR' | 'TRACKING' | 'DRAW_OBJECTS';

@injectable()
class DrawingStore {
    @observable
    public drawingMode: DRAWING_MODE = 'NONE';

    @observable
    public object: MapObject | null;

    @observable
    public onPause: boolean = true;

    @computed
    public get showDistanceControl(): boolean {
        return this.drawingMode === 'DISTANCE_CALCULATOR';
    }

    @computed
    public get isTracking(): boolean {
        return this.drawingMode === 'TRACKING';
    }

    @computed
    public get showDrawControl(): boolean {
        return this.drawingMode === 'DRAW_OBJECTS';
    }
}

export { DrawingStore };
