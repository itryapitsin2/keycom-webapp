import { injectable } from 'inversify';
import { action, computed, observable } from 'mobx';

import { FavWindowElement } from './fav-window-element';

@injectable()
export class FavWindowElementStore {
    @observable private _elements: FavWindowElement[] = [];

    @computed
    public get elements(): FavWindowElement[] {
        return this._elements;
    }

    @action
    public setElements(elements: FavWindowElement[]) {
        this._elements = elements;
    }

    @action
    public setElement(element: FavWindowElement) {
        this._elements.push(element);
    }

    public getElementByKey(key: string) {
        return this.elements.find((el: FavWindowElement) => {
            return el.key === key;
        });
    }
}
