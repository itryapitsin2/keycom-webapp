import { observable } from 'mobx';

interface FavWindowElementItem {
    title: string;
    state: any;
}

export class FavWindowElement {
    public readonly key: string;
    public readonly iconClassName?: string;
    public readonly action: Function;
    /**
     * Action parameters
     */
    @observable public readonly params?: FavWindowElementItem[];

    public constructor(
        key: string,
        action: Function,
        params?: FavWindowElementItem[],
        iconClassName?: string,
    ) {
        this.key = key;
        this.action = action;
        this.iconClassName = iconClassName;
        this.params = params;
    }
}
