import * as L from 'leaflet';
import { action, computed, observable } from 'mobx';
import { LeafletObject, MapObject, MapObjectType } from './map-object';

export class Line implements MapObject {
    public type: MapObjectType = 'LINE';

    @observable
    public coords: L.LatLng[] = [];

    @observable
    public dynamicLastPoint: L.LatLng;

    public text: string;

    public description: string;

    public get leafletObject(): LeafletObject | null {
        if (this.dynamicLastPoint) {
            return L.polyline([...this.coords, this.dynamicLastPoint]);
        }
        return L.polyline(this.coords);
    }

    @action
    public setLastPoint(latlng: L.LatLng): void {
        this.coords.push(latlng);
        this.dynamicLastPoint = null;
    }

    @computed
    public get length(): number {
        let distance = 0;
        let previousLatLng = null;

        this.coords.forEach((latlng: L.LatLng): void => {
            if (previousLatLng == null) {
                previousLatLng = latlng;
            } else {
                distance += latlng.distanceTo(previousLatLng);
                previousLatLng = latlng;
            }
        });

        if (this.dynamicLastPoint && previousLatLng) {
            distance += this.dynamicLastPoint.distanceTo(previousLatLng);
        }
        return distance;
    }
}
