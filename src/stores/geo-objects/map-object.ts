import * as L from 'leaflet';

export type Marker = L.Marker;
export type Polyline = L.Polyline;
export type LeafletObject = Marker | Polyline;

export type MapObjectType = 'POINT' | 'LINE' | 'TEXT';

export interface MapObject {
    coords: L.LatLng | L.LatLng[];
    setLastPoint(latlng: L.LatLng);
    type: MapObjectType;
    readonly leafletObject: LeafletObject | null;
}
