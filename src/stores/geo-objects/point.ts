import * as L from 'leaflet';
import { LeafletObject, MapObject, MapObjectType } from './map-object';

export class Point implements MapObject {
    public type: MapObjectType = 'POINT';

    public coords: L.LatLng;

    public text: string;

    public description: string;

    public get leafletObject(): LeafletObject | null {
        if (this.coords) {
            return L.marker(this.coords);
        }
        return null;
    }

    public setLastPoint(latlng: L.LatLng): void {
        this.coords = latlng;
    }
}
