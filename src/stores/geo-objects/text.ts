import * as L from 'leaflet';
import { NotImplemented } from '../../../utils/exceptions';
import { LeafletObject, MapObject, MapObjectType } from './map-object';

export class Text implements MapObject {
    public type: MapObjectType = 'TEXT';

    public coords: L.LatLng;

    public text: string;

    public fontSize: string;

    public get leafletObject(): LeafletObject | null {
        throw NotImplemented(`Text.leafletObject ${this.constructor.name}`);
    }

    public setLastPoint(latlng: L.LatLng): void {
        throw NotImplemented(`Text.setCoords ${this.constructor.name} ${latlng}`);
    }
}
