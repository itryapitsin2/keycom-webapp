import { LatLngExpression } from 'leaflet';
import { computed, observable } from 'mobx';
import { injectable } from 'inversify';

@injectable()
export class TrackerStore {
    @observable
    public points: LatLngExpression[] = [];

    @observable
    public onPause: boolean = true;

    @computed
    public get showTracker(): boolean {
        return !this.onPause;
    }
}
