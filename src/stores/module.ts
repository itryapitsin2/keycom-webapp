import { ContainerModule, interfaces } from 'inversify';
import { QuickAddressSearchWindowStore } from './windows/quick-address-search-window-store';
import { FavWindowElementStore } from './footer/fav-window-element-store';

export const StoreModule = new ContainerModule((bind: interfaces.Bind): void => {
    QuickAddressSearchWindowStore.register(bind);

    bind(FavWindowElementStore)
        .toSelf()
        .inSingletonScope();
});
