import { interfaces } from 'inversify';

export interface SerializableStore {
    serialize: () => any;

    deserialize: (json: any) => void;
}

export interface SelfRegisteredStore {
    new (): SerializableStore;
    register: (bind: interfaces.Bind) => void;
    readonly KEY: string;
}

export const staticImplements = <T>() => <U extends T>(constructor: U) => {
    constructor;
};
