import 'reflect-metadata';
import { action, computed, observable, transaction } from 'mobx';
import { injectable, interfaces } from 'inversify';
import { Address } from '../../api/models/address';
import { getDefaultModelSchema, serializable, serialize, update } from 'serializr';
import { SerializableStore, SelfRegisteredStore, staticImplements } from '../serializable-store';

@injectable()
@staticImplements<SelfRegisteredStore>()
export class QuickAddressSearchWindowStore implements SerializableStore {
    public static readonly KEY: string = 'QuickAddressSearchWindowStore';

    @observable private _selectedItem: Address;

    @observable private _items: Address[] = [];

    @serializable @observable private _showing: boolean = false;

    @observable private _loading: boolean = false;

    @serializable @observable private _pattern: string = '';

    @observable private _favWindowTitle: string = '';

    @observable private _showingTitleDialog: boolean = false;

    @action public setItems = (items: Address[]): void => {
        this._items = items;
    };

    @action public show = (): void => {
        this._showing = true;
    };

    @action public hide = (): void => {
        this._showing = false;
    };

    @action public showTitleDialog = (): void => {
        this._showingTitleDialog = true;
    };

    @action public hideTitleDialog = (): void => {
        this._showingTitleDialog = false;
    };

    @action public startLoading = (): void => {
        this._loading = true;
    };

    @action public stopLoading = (): void => {
        this._loading = false;
    };

    @action public selectItem = (item: Address): void => {
        this._selectedItem = item;
        this._pattern = item.name;
    };

    @action public setPattern = (pattern: string): void => {
        this._pattern = pattern;
    };

    @action public setFavWindowTitle = (title: string): void => {
        this._favWindowTitle = title;
    };

    @computed
    public get pattern(): string {
        return this._pattern;
    }

    @computed
    public get favWindowTitle(): string {
        return this._favWindowTitle;
    }

    @computed
    public get isShow(): boolean {
        return this._showing;
    }

    @computed
    public get isShowTitleDialog(): boolean {
        return this._showingTitleDialog;
    }

    @computed
    public get isLoading(): boolean {
        return this._loading;
    }

    @computed
    public get items(): Address[] {
        return this._items;
    }

    @computed
    public get selectedItem(): Address {
        return this._selectedItem;
    }

    public serialize = (): any => {
        return serialize(getDefaultModelSchema(QuickAddressSearchWindowStore), this);
    };

    public deserialize = (json: any): void => {
        transaction((): void => {
            update(getDefaultModelSchema(QuickAddressSearchWindowStore), this, json);
        });
    };
    public static register = (bind: interfaces.Bind): void => {
        bind(QuickAddressSearchWindowStore)
            .toSelf()
            .inSingletonScope();
    };
}
