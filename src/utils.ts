import { withNaming } from '@bem-react/classname';

export const className = withNaming({ n: 'keypro--', e: '__', m: '_' });
