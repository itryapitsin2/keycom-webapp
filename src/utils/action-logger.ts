import { spy } from 'mobx';

interface IChange {
    type: string;
    name: string;
    arguments: never;
}

spy((event: IChange): void => {
    if (event.type === 'action') {
        console.log(`${event.name} with args:`);
        console.log(event.arguments);
    }
});
