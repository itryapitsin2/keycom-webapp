// @flow

export const INFO = 'color: #0066cc';
export const ERROR = 'color: #ff0000';
export const WARNING = 'color: #ffcc00';
