export type Direction = 'W' | 'S' | 'E' | 'N';

export interface DMS {
    direction: Direction;
    deg: number;
    min: number;
    sec: number;
}

export function convertDDToDMS(deg: number, lngOrLat: 'lng' | 'lat'): DMS {
    let d = Math.floor(deg);
    const minFloat = (deg - d) * 60;
    let m = Math.floor(minFloat);
    const secFloat = (minFloat - m) * 60;
    let s = Math.round(secFloat);
    // After rounding, the seconds might become 60. These two
    // if-tests are not necessary if no rounding is done.
    if (s === 60) {
        m += 1;
        s = 0;
    }
    if (m === 60) {
        d += 1;
        m = 0;
    }

    let direction = null;
    if (lngOrLat === 'lng') {
        direction = deg > 0 ? 'E' : 'W';
    } else {
        direction = deg > 0 ? 'N' : 'S';
    }

    return {
        direction,
        deg: d,
        min: m,
        sec: s,
    };
}
