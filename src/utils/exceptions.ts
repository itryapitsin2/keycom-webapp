// @flow

import { ERROR } from './color';

export const NotImplemented = (methodName: string): void =>
    console.log('%c Not implemented method,', ERROR, methodName);
// export const NotAuthorizedException = () => {};
