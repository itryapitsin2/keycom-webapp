const http = async <T = {}>(request: RequestInfo): Promise<T> => {
    return new Promise((resolve: never): void => {
        try {
            fetch(request)
                .then((response: Response): {} => response.json())
                .then(resolve)
                .catch(console.log);
        } catch (e) {
            console.log(e);
        }
    });
};

export default http;
