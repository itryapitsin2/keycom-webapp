import L from 'leaflet';
import { Lambda } from 'mobx';

export interface ElementToAction {
    action: Lambda;
    eventName: string;
    elementId: string;
}

export class PropagationUtils {
    public static disableControlClickPropagation(element: HTMLElement): void {
        if (!L.Browser.touch || L.Browser.ie) {
            L.DomEvent.disableClickPropagation(element);
            L.DomEvent.on(element, 'mousewheel', L.DomEvent.stopPropagation);
        } else {
            L.DomEvent.on(element, 'click', L.DomEvent.stopPropagation);
            L.DomEvent.on(element, 'dblclick', L.DomEvent.stopPropagation);
        }
    }

    public static enableControlClickPropagation(element: HTMLElement): void {
        if (!L.Browser.touch || L.Browser.ie) {
            L.DomEvent.disableClickPropagation(element);
            L.DomEvent.off(element, 'mousewheel', L.DomEvent.stopPropagation);
        } else {
            L.DomEvent.off(element, 'click', L.DomEvent.stopPropagation);
        }
    }

    public static bindActionToElement(dict: ElementToAction[]): void {
        dict.forEach((item: ElementToAction): void => {
            const element = L.DomUtil.get(item.elementId);
            if (element) {
                PropagationUtils.disableControlClickPropagation(element);
                L.DomEvent.on(element, item.eventName, item.action);
            }
        });
    }

    public static unbindActionToElement(dict: ElementToAction[]): void {
        dict.forEach((item: ElementToAction): void => {
            const element = L.DomUtil.get(item.elementId);
            if (element) {
                PropagationUtils.enableControlClickPropagation(element);
                L.DomEvent.off(element, item.eventName, item.action);
            }
        });
    }
}
