export const SERVER_ADDRESS = 'localhost';
export const PORT = '8000';
export const PROTOCOL = 'http';
export const URL = `${PROTOCOL}://${SERVER_ADDRESS}:${PORT}`;
export const MINIMUM_CHARS_FOR_SEARCHING = 3;
