export type KeyEventHandler<T> = (event: T) => void;
